# Dockerfile

# FROM directive instructing base image to build upon
FROM python:3-onbuild

# COPY startup script into known file location in container
COPY docker_entrypoint.sh /docker_entrypoint.sh

# EXPOSE port 1800 to allow communication to/from server
EXPOSE 1800

# CMD specifcies the command to execute to start the server running.
ENTRYPOINT ["/docker_entrypoint.sh"]
# done!