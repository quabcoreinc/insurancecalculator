from django.shortcuts import render
from app.service.appservice import AppService
from app.service.serviceresponses import GetDashboardDataResponse, GetCarInsuranceDetailsResponse
from app.common.logging import LogHelper

import logging
log = logging.getLogger(__name__)
_as = AppService()


def index(request):
    #log ip of client accessing the app
    log.info("[WEB] Admin dashboard accessed from ip: {}".format(LogHelper.get_ip_from_request(request)))

    response = GetDashboardDataResponse()
    try:
        #call service method for dashboard data
        response = _as.get_dashboard_data()
    except Exception as ex:
        #log error details incase the service fails
        log.error("{} - ip {}" .format(str(ex), LogHelper.get_ip_from_request(request)))

    return render(request, "dashboard.html", {'data': response.DashboardView})


def car_details(request, car_id):
    #log ip of client accessing the app
    log.info("[WEB] Car [{}] details viewed from ip: {}".format(car_id, LogHelper.get_ip_from_request(request)))

    response = GetCarInsuranceDetailsResponse()

    try:
        #call service method for car details
        response = _as.get_car_insurance_details(car_id)
    except Exception as ex:
        #log error details incase the service fails
        log.error("{} - ip {}" .format(str(ex), LogHelper.get_ip_from_request(request)))

    return render(request, "cardetails.html", {'data': response.CarInsuranceDetailsView})
