
class InsuranceCalculator:

    def __init__(self):
        pass

    def calculate_third_party_premium(self, insurance_detail, insurance_additions):

        #Calculate and assign the equivalent amounts for the percetages
        fleet_discount = (insurance_detail.fleet_discount /100) * insurance_detail.tp_basic_premium
        other_loadings = insurance_additions.sum()

        inexperienced_driver_loading = (insurance_detail.inexperienced_driver_loading /100) * insurance_detail.tp_basic_premium
        old_age_loadings = (insurance_detail.old_age_loadings /100) * insurance_detail.tp_basic_premium
        ncd = (insurance_detail.ncd /100) * (insurance_detail.tp_basic_premium + old_age_loadings)

        return insurance_detail.tp_basic_premium + insurance_detail.extra_seat + old_age_loadings + inexperienced_driver_loading \
               + insurance_detail.extra_tppd_cover + other_loadings - ncd - fleet_discount

    def calculate_comprehensive_premium(self, insurance_detail, insurance_additions):

        #Calculate and assign the equivalent amounts for the percentages
        own_damage_basic_premium = (insurance_detail.own_damage_rate /100) * insurance_detail.sum_insured
        cc_loading = (insurance_detail.own_damage_rate /100) * own_damage_basic_premium
        old_age_loadings = (insurance_detail.old_age_loadings /100) * own_damage_basic_premium

        inexperienced_driver_loading = (insurance_detail.inexperienced_driver_loading /100) * own_damage_basic_premium
        other_loadings = insurance_additions.sum()
        excess_brought_back = (insurance_detail.excess_brought_back /100) * own_damage_basic_premium

        #calculate comprehensive basic compaonent from premium
        comprehensive_basic = own_damage_basic_premium + cc_loading + old_age_loadings\
                              + insurance_detail.tp_basic_premium

        ncd = (insurance_detail.ncd /100) * comprehensive_basic
        fleet_discount = (insurance_detail.fleet_discount /100) * comprehensive_basic

        return comprehensive_basic + inexperienced_driver_loading + excess_brought_back + insurance_detail.extra_seat\
               + insurance_detail.extra_tppd_cover + other_loadings - ncd - fleet_discount



