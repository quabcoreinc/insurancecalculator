from rest_framework import routers
from .views import InsuranceViewSet

router = routers.DefaultRouter()
router.register(r'calculate_insurance', InsuranceViewSet)
