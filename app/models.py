from django.db import models
import uuid


class ApiRequest(models.Model):
    request_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(max_length=25)
    car_id = models.CharField(max_length=50)
    created_on = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "api_requests"


class ApiResponse(models.Model):
    response_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(max_length=25)
    insurance_price = models.CharField(max_length=100)
    car_id = models.CharField(max_length=50)
    created_on = models.DateTimeField(auto_now_add=True)
    request_id = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "api_responses"


class Car(models.Model):
    car_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    insured_by = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    contact_no = models.CharField(max_length=15)
    vehicle_make = models.CharField(max_length=150)
    vehicle_cc = models.CharField(max_length=20)
    registration_no = models.CharField(max_length=20)
    chassis = models.CharField(max_length=150)
    engine_no = models.CharField(max_length=150)
    year = models.IntegerField()
    no_of_seats = models.IntegerField()
    licence_issue_year = models.IntegerField()
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(null=True)
    is_active = models.BooleanField(default=True)
    insurance = models.ForeignKey("InsuranceDetail")

    class Meta:
        db_table = "cars"


class InsuranceDetail(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    insurance_type = models.CharField(max_length=20)
    sum_insured = models.DecimalField(max_digits=15, decimal_places=2)
    own_damage_rate = models.DecimalField(max_digits=4, decimal_places=2)
    cc_loadings = models.DecimalField(max_digits=4, decimal_places=2)
    old_age_loadings = models.DecimalField(max_digits=4, decimal_places=2)
    inexperienced_driver_loading = models.DecimalField(max_digits=10, decimal_places=2)

    tp_basic_premium = models.DecimalField(max_digits=10, decimal_places=2)
    extra_seat = models.DecimalField(max_digits=10, decimal_places=2)
    tppdl = models.DecimalField(max_digits=10, decimal_places=2)

    ncd = models.DecimalField(max_digits=4, decimal_places=2)
    fleet_discount = models.DecimalField(max_digits=4, decimal_places=2)

    excess_brought_back = models.DecimalField(max_digits=4, decimal_places=2)
    extra_tppd_cover = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        db_table = "insurance_details"


class InsuranceAddition(models.Model):
    unique_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    additional_peril = models.DecimalField(max_digits=10, decimal_places=2)
    ecowas_peril = models.DecimalField(max_digits=10, decimal_places=2)
    pa_benefits = models.DecimalField(max_digits=10, decimal_places=2)
    nic_nhis_ecowas = models.DecimalField(max_digits=10, decimal_places=2)
    is_active = models.BooleanField(default=True)

    def sum(self):
        return self.additional_peril + self.ecowas_peril + self.pa_benefits + self.nic_nhis_ecowas

    class Meta:
        db_table = "insurance_additions"

