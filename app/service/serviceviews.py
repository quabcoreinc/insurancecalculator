#specialize class for return results after calculting premium
class CalculatedInsuranceView:
    type = ''
    insurance_price = 0
    car_id = ''


#specialize classes for populating dashboard

class CarDetailsView:
    car_id = ''
    car_model = ''
    cc = ''
    chassis = ''
    year = ''
    registration_no = ''
    insurance_type = ''


class CarInsuranceDetailsView(CarDetailsView):
    insured_by = ''
    address = ''
    contact_no = ''
    vehicle_cc = ''
    engine_no = ''
    no_of_seats = 0
    licence_issue_year = 0
    sum_insured = 0
    own_damage_rate = 0
    cc_loadings = 0
    old_age_loadings = 0
    inexperienced_driver_loading = 0
    tp_basic_premium = 0
    extra_seat = 0
    tppdl = 0
    ncd = 0
    fleet_discount = 0
    excess_brought_back = 0
    extra_tppd_cover = 0


class DashboardView:
    no_of_requests = 0
    no_of_responses = 0
    no_of_vehicles = 0
    no_of_comprehensives = 0
    no_of_third_parties = 0
    no_of_insured_cars = 0

    additional_peril = 0.00
    ecowas_peril = 0.00
    pa_benefits = 0.00
    nic_nhis_ecowas = 0.00

    cars = None
