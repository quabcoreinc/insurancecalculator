#!/bin/sh

# Start Gunicorn processes
echo Starting Gunicorn.
exec gunicorn InsuranceCalculator.wsgi:application \
    --bind 0.0.0.0:1800 \
    --workers 3